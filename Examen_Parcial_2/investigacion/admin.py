from django.contrib import admin
from .models import Investigacion, Categoria
# Register your models here.
admin.site.register(Investigacion)
admin.site.register(Categoria)
