from django.shortcuts import render

from .models import Investigacion, Categoria

from django.views import generic
from .forms import InvestigacionForm
from django.urls import reverse_lazy

# Create your views here.
# RETRIEVE

def List(request):
	object_list = Investigacion.objects.all()
	categorias = Categoria.objects.all()
	if request.method == 'POST':
		cat = request.POST.get('categoria')
		if(cat is not None):
			categoria = Categoria.objects.get(nombre = cat)
			object_list = Investigacion.objects.filter(categoria = categoria )
	context = {  
	'categorias': categorias,
	'object_list' : object_list,
	}
	return render(request , 'investigacion/list.html' , context)

class Detail(generic.DetailView):
	template_name = "investigacion/detail.html"
	model = Investigacion

class Create(generic.CreateView):
	template_name = "investigacion/create.html"
	model = Investigacion
	form_class = InvestigacionForm
	success_url = reverse_lazy("inv_list")

	def form_valid(self , form ):
		obj = form.save(commit = False)
		obj.investigador = self.request.user 
		return super(Create , self).form_valid(form)


class Update(generic.UpdateView):
	template_name = "investigacion/update.html"
	model = Investigacion
	fields = [
			"titulo",
			"categoria",
			"num_paginas",
			"slug",
		]
	success_url = reverse_lazy("inv_list")
