from django import forms 

from django.contrib.auth.forms import UserCreationForm

class LoginForm(forms.Form):
	username = forms.CharField(max_length=24, widget=forms.TextInput())
	password = forms.CharField(max_length=24, widget=forms.PasswordInput())

class Users_Form(UserCreationForm):	
	email = forms.CharField(max_length=48)