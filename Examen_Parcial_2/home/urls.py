from django.urls import path
from home import views
from django.contrib.auth.views import logout_then_login

urlpatterns = [
	path('', views.Login, name="login"),
	path('signup/', views.Signup.as_view(), name="signup"),
	path('cerrar/' , logout_then_login  , name = 'logout' ),
]