from django.shortcuts import render

from django.contrib.auth import authenticate , login

from .forms import Users_Form , LoginForm
from django.urls import reverse_lazy
from django.views import generic
from .forms import LoginForm

# Create your views here.
def Login(request):
	message = "Not Login"
	form = LoginForm(request.POST or None)
	if request.method == "POST":
		form = LoginForm(request.POST or None)
		if form.is_valid():
			username = request.POST["username"]
			password = request.POST["password"]
			user = authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					login(request , user)
					message = "User Logged"
				else:
					message = "User is Not Active"
			else:
				message = "Username or Password is not Correct"
	context = {
		"form": form,
		"message": message
	}
	return render(request, "home/login.html", context)


class  Signup(generic.FormView):
	template_name = 'home/signup.html'
	form_class = Users_Form
	success_url = reverse_lazy('login')

	def form_valid(self , form):
		user = form.save()
		return super(Signup, self).form_valid(form) 
