from django.urls import path 

from software import views

urlpatterns = [
    path('list/', views.List.as_view() , name = 'soft_list'),  
 	path('createdep/', views.Create_Dep.as_view() , name = 'dep_create'),  
    path('createsoft/', views.Create.as_view() , name = 'soft_create'),  
         
]