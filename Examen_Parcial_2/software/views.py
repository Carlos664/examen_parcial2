from django.shortcuts import render
from django.db.models import Q

from .models import Software, Departamento
from .forms import SoftwareForm , DepartamentoForm
from django.views import generic
from django.urls import reverse_lazy


class List(generic.ListView):
	template_name = "software/list_soft.html"
	queryset = Software.objects.all()

	def get_queryset(self, *args, **kwargs):
		qs = Software.objects.all()
		query = self.request.GET.get("q", None)
		if query is not None:
			qs = qs.filter(Q(departamento__nombre__icontains=query) | Q(nombre__icontains=query) )
		return qs

class Create(generic.CreateView):
	template_name = "software/create_soft.html"
	model = Software
	form_class = SoftwareForm
	success_url = reverse_lazy("soft_list")

class Create_Dep(generic.CreateView):
	template_name = "software/create_dep.html"
	model = Departamento
	form_class = DepartamentoForm
	success_url = reverse_lazy("soft_list")


