from django.db import models

# Create your models here.


class Departamento(models.Model):
	nombre = models.CharField(max_length = 50)	
	def __str__(self):
		return self.nombre


class Software(models.Model):
	nombre = models.CharField(max_length=24)
	departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
	funcion = models.TextField()
	fecha = models.DateField(auto_now_add=True)
	slug = models.SlugField()

	def __str__(self):
		return self.nombre