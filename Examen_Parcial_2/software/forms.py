from django import forms

from .models import Software, Departamento

class SoftwareForm(forms.ModelForm):
	class Meta:
		model = Software
		fields = [
			"nombre",
			"departamento",
			"funcion",
			"slug",
		]

class DepartamentoForm(forms.ModelForm):
	class Meta:
		model = Departamento
		fields = [
			"nombre",
		]
